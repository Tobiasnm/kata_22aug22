import java.util.regex.Pattern;

public class main {
    public static void main(String[] args) {
        System.out.println(TranslateSingleWordToPigEnglish("have"));
        System.out.println(TranslateSingleWordToPigEnglish("cram"));
        System.out.println(TranslateSingleWordToPigEnglish("take"));
        System.out.println(TranslateSingleWordToPigEnglish("cat"));
        System.out.println(TranslateSingleWordToPigEnglish("shrimp"));
        System.out.println(TranslateSingleWordToPigEnglish("trebuchet"));
        System.out.println(TranslateSingleWordToPigEnglish("ate"));
        System.out.println(TranslateSingleWordToPigEnglish("apple"));
        System.out.println(TranslateSingleWordToPigEnglish("oaken"));
        System.out.println(TranslateSingleWordToPigEnglish("eagle"));
    }

    public static String TranslateSingleWordToPigEnglish(String word){
         if(Pattern.compile("[aeiouAEIOU]").matcher(Character.toString(word.charAt(0))).find()){
            return word + "yay";
        }
        else {
            String vowels = "aeiouAEIOU";
            for(int i = 0; i < word.length(); i++){
                if(vowels.contains(Character.toString(word.charAt(i)))) {
                    return word.substring(i) + word.substring(0, i) + "ay";
                }
            }
        }
        return word;
    }
}
